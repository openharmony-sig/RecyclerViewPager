/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

import { verticalViewPager } from "@ohos/recyclerviewpager"
import { Toolbar } from './toolbar'
import { display } from '@kit.ArkUI'


@Entry
@ComponentV2
@Preview
struct verticalViewPagerDemoActivity {
  @Local index: number = 0
  @Local offsetY: number = 0
  private arr: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9]
  private topSpaced: number = 35 * 2
  private marginBottom = 100;
  private marginTop = 100;
  private fontSize: number = 30 * 2
  private ContainderHeight: number = px2vp(display.getDefaultDisplaySync().height)
  private ContainderWidth: number | string = '100%'
  private offsetFlag: number = 100
  private pageFlag: boolean = false
  private ScreenOffset: number = 3000

  @Builder specificParam(item : ESObject) {

    if (item.i == this.index) {
      Flex() {
        Text("item=" + this.index).fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ top: this.marginTop, bottom: this.marginBottom /*, left: this.topSpaced, right: this.topSpaced */
      })
      .width('100%')
      .height(this.ContainderHeight - this.marginBottom - this.marginTop)
      .backgroundColor("#273238")
      .scale({
        x: this.offsetY < 0 ? 1 + this.offsetY / this.ScreenOffset : 1 - this.offsetY / this.ScreenOffset,
        y: 1
      })
    } else {
      Flex() {
        Text("item").fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ top: this.marginTop, bottom: this.marginBottom, left: this.topSpaced, right: this.topSpaced })
      .width('90%')
      .height(this.ContainderHeight - this.marginBottom - this.marginTop)
      .backgroundColor("#273238")
      .scale({
        x: this.offsetY < 0 ? 1 - this.offsetY / this.ScreenOffset : 1 + this.offsetY / this.ScreenOffset,
        y: 1
      })
    }

  }

  build() {
    Column() {
      Toolbar({ title: 'Vertical ViewPager Demo', isBack: true })
      Flex() {
        verticalViewPager({
          arr: this.arr,
          offsetY: this.offsetY!!,
          index: this.index!!,
          marginTop: this.marginTop,
          marginBottom: this.marginBottom,
          ContainderWidth: this.ContainderWidth,
          ContainderHeight: this.ContainderHeight - this.marginBottom - this.marginTop,
          content: (item : ESObject) => {
            this.specificParam(item)
          }
        })
      }
    }
  }
}
